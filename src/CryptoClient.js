import Postmate from "postmate";
import {CryptoError, InitResult, SignResult, CertificatesResult, CryptoCertificate} from "crypto-types";

const CALLBACK = {
    INIT_CALLBACK: 'INIT_CALLBACK',
    SIGN_CALLBACK: 'SIGN_CALLBACK',
    CERTIFICATE_CALLBACK: 'CERTIFICATE_CALLBACK'
};

export default class CryptoClient {

    /**
     * Метод для инициализации
     * В рамках работы метода происходит загрузка информации о пакете + проверка рабочего места
     * @param {string} url - Базовый URL
     * @param {string} id - Идентификатор пакета
     * @param {string} element - Идентификатор элемента DOM, в котором будет размещен невидимый IFrame
     * @return Promise.<InitResult>
     */
    async init(url, id, element) {
        const self = this;
        self.id = id;
        self.url = url;

        const handshake = new Postmate({
            url: url,
            name: '',
            container: document.getElementById(element)
        });

        const messageId = Date.now().toString();
        let promiseResolve, promiseReject;
        const promise = new Promise(function (resolve, reject) {
            promiseResolve = resolve;
            promiseReject = reject;
        });

        await handshake.then(child => {
            self.iframe = child;

            self.iframe.on(CALLBACK.INIT_CALLBACK, data => {
                if (data === undefined || !data.hasOwnProperty('id')){
                    return;
                }
                if (data.id !== messageId){
                    return;
                }
                promiseResolve(new InitResult(data.errors, data.data));
            });

            self.iframe.call('init', {id: messageId, packageId: id});
        });
        return promise;
    }

    /**
     * Метод для получения списка сертификатов
     * @return Promise.<CertificatesResult>
     */
    certificates() {
        if (this.iframe === undefined){
            let result = new CertificatesResult(true, [new CryptoError('1', 'Не инициализирован', '')]);
            return Promise.resolve(result);
        }

        const messageId = Date.now().toString();
        let promiseResolve, promiseReject;
        const promise = new Promise(function (resolve, reject) {
            promiseResolve = resolve;
            promiseReject = reject;
        });

        this.iframe.on(CALLBACK.CERTIFICATE_CALLBACK, response => {
            if (response === undefined || !response.hasOwnProperty('id')){
                return;
            }
            if (response.id !== messageId){
                return;
            }
            if (!response.hasOwnProperty('data')){
                let error = new CryptoError('', 'Список сертификатов пуст');
                promiseResolve(new CertificatesResult([error], []));
            }
            let certificates = [];
            for(let certificate of response.data){
                certificates.push(new CryptoCertificate(certificate));
            }
            promiseResolve(new CertificatesResult(response.errors, certificates));
        });
        this.iframe.call('certificates', {id: messageId});
        return promise;
    }

    /**
     * Метод для формирования подписи
     * @param {CryptoCertificate} certificate - Сертификат для подписания
     * @return Promise.<SignResult>
     */
    sign(certificate) {
        if (this.iframe === undefined){
            let result = new SignResult();
            return Promise.resolve(result);
        }

        const messageId = Date.now().toString();
        let promiseResolve, promiseReject;
        const promise = new Promise(function (resolve, reject) {
            promiseResolve = resolve;
            promiseReject = reject;
        });

        this.iframe.on(CALLBACK.SIGN_CALLBACK, data => {
            if (data === undefined || !data.hasOwnProperty('id') || !data.hasOwnProperty('data')){
                return;
            }

            if (data.id !== messageId){
                return;
            }

            promiseResolve(new SignResult(data.errors));
        });
        this.iframe.call('sign', {id: messageId, packageId: this.id, certificate: certificate});
        return promise;
    }
}

